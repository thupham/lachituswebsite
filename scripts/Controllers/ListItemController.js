/**
 * Created by phaml_000 on 12/17/2015.
 */
App.controller('ListItemController', function ($scope, $window, $stateParams, $state, $uibModal) {
    $scope.changeItemInRow = changeItemInRow;
    $scope.openFilterModal = openFilterModal;

    var itemInRow = $scope.itemInRow = $stateParams.itemInRow;
    var items = $scope.items = [
        {
            name:"Adipisicing Elit",
            image:"images/items/01.jpg",
            price:"169.00 USD",
            isNew: true
        },
        {
            name:"Adipisicing",
            image:"images/items/02.jpg",
            price:"169.00 USD",
            isNew: false
        },
        {
            name:"Elit",
            image:"images/items/03.jpg",
            price:"169.00 USD",
            isNew: true
        },
        {
            name:"Adipisicing Elit",
            image:"images/items/04.jpg",
            price:"169.00 USD",
            isNew: false
        },
        {
            name:"Adipisicing",
            image:"images/items/05.jpg",
            price:"169.00 USD",
            isNew: true
        },
        {
            name:"Elit",
            image:"images/items/06.jpg",
            price:"169.00 USD",
            isNew: true
        },
        {
            name:"Adipisicing",
            image:"images/items/07.jpg",
            price:"169.00 USD",
            isNew: false
        },
        {
            name:"Elit",
            image:"images/items/08.jpg",
            price:"169.00 USD",
            isNew: true
        },
        {
            name:"Adipisicing Elit",
            image:"images/items/09.jpg",
            price:"169.00 USD",
            isNew: true
        },
        {
            name:"Adipisicing",
            image:"images/items/01.jpg",
            price:"169.00 USD",
            isNew: true
        },
        {
            name:"Elit",
            image:"images/items/01.jpg",
            price:"169.00 USD",
            isNew: true
        },
        {
            name:"Adipisicing",
            image:"images/items/01.jpg",
            price:"169.00 USD",
            isNew: true
        }
    ];

    function changeItemInRow(n){
        $scope.itemInRow = n;
        $state.go($state.current.name, {
            itemInRow: $scope.itemInRow,
            category: 'new'
        });
    }

    function openFilterModal(){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'FilterModalContent.html',
            size: 'lg',
            resolve: {}
        });

        modalInstance.result.then(function (user) {
            $scope.user = user;
            $timeout(function(){$scope.user = ""}, 30000);
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    }
});